#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <linux/sched.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <sodium.h>

cons:t char *const port = "3490"; // the port client will be connecting to
const int MAX_DATA_LEN = 100;
const char *arg0;

void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET)
		return &(((struct sockaddr_in*)sa)->sin_addr);

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int g:et_sock_or_exit(const struct addrinfo hints, const char *const ip)
{
	struct addrinfo *server_info, *p;
	{
		int error;
		if ((error = getaddrinfo(ip, port, &hints, &server_info)) != 0) {
			fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(error));
			exit(1);
		}
	}
	int last_err, fd;
	/* loop through all the results and connect to the first we can */
	for (p = server_info; p != NULL; p = p->ai_next) {
		if (-1 == (fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol))) {
			last_err = errno;
			continue;
		}
		if (-1 == connect(fd, p->ai_addr, p->ai_addrlen)) {
			close(fd);
			last_err = errno;
			continue;
		}
		break;
	}
	if (p == NULL) {
		fprintf(stderr, "client: failed to connect\n");
		fprintf(stderr, "%s\n", strerror(last_err));
		exit(2);
	}

	char s[INET6_ADDRSTRLEN];
	inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr), s, sizeof s);
	printf("client: connecting to %s\n", s);

	freeaddrinfo(server_info);
	return fd;
}

void usage(void)
{
	fprintf(stderr,"%s encrypts a connection to host.\n"
	"usage: %s host\n", arg0, arg0);
	exit(1);
}

int recv_till_exit(int sock, crypto_secretstream_xchacha20poly1305_state *state) {
	while(1) {
		char hex[256];
		unsigned char tag;
		unsigned char buf[65], cbuf[82];
		unsigned long long mlen_p;
		size_t count = 0;
		int res = 0;
		puts("waiting for message");
		count = recv(sock, cbuf, sizeof(cbuf), 0);
		if(count == (size_t) -1 || count == 0) {
			perror("recv()");
			return 0;
		}
		printf("recv count %d\n", count);
		sodium_bin2hex(hex, sizeof(hex), cbuf, count);
		printf("received: %s\n", hex);

		res = crypto_secretstream_xchacha20poly1305_pull(
			state, buf, &mlen_p, &tag,
			cbuf, sizeof(buf) + crypto_secretstream_xchacha20poly1305_ABYTES,
			NULL, 0);
		if(res != 0) {
			puts("invalid ciphertext");
			/* Invalid/incomplete/corrupted ciphertext - abort */
		}
		unsigned char unpadded_len = buf[0];
		printf("\ntag: %d\nmlen_p: %d\nunpadded_len: %d\n", tag, mlen_p, unpadded_len);
		sodium_bin2hex(hex, sizeof(hex), buf + 1, unpadded_len);
		printf("hex: %s\ngot output '%.*s'\n", hex, unpadded_len, buf + 1);

		assert(tag == 0);
	fflush(stdout);
	}
	return 0;
}

int send_till_quit(int conn, crypto_secretstream_xchacha20poly1305_state *state) {
	/* input buffer message buffer ciphertext buffer */
	unsigned char ibuf[64], mbuf[65], cbuf[128];

	while(ibuf[0] != ':' && ibuf[1] != 'q') {
		/*
		 * fgets() always null terminates.
		 * fgets() always stores the whole string.
		 */
		unsigned char m_len = 0;

		memset(ibuf, 0xFF, sizeof(ibuf));
		puts("waiting on input");
		ssize_t r = read(STDIN_FILENO, ibuf, sizeof(ibuf) - 1);
		if(r == (ssize_t) -1) {
			perror("read()");
			return 1;
		}
		ibuf[r] = '\0';
		printf("got input '%s'\n", ibuf);
		m_len = strlen(ibuf);
		assert(m_len < 65);
		mbuf[0] = m_len;
		memcpy(mbuf + 1, ibuf, sizeof(mbuf) - 1);


		crypto_secretstream_xchacha20poly1305_push
			(state, cbuf, NULL, mbuf, sizeof(mbuf), NULL, 0, 0);
		puts("sending");
		send(conn, cbuf, sizeof(mbuf) + crypto_secretstream_xchacha20poly1305_ABYTES , 0);
		//TODO: check for error on send and exit.
		puts("done sending");
		char hex[256];
		sodium_bin2hex(hex, sizeof(hex), cbuf, sizeof(mbuf) + crypto_secretstream_xchacha20poly1305_ABYTES);
		printf("sending: %s\n", hex);
	}
	return 0;
}

void do_salt_things(int sock) {
	if(sodium_init() < 0) {
		fprintf(stderr, "libsodium could not init.\n");
		exit(1);
	}

	char hex[256];
	unsigned char our_pk[crypto_kx_PUBLICKEYBYTES];
	unsigned char our_sk[crypto_kx_SECRETKEYBYTES];

	unsigned char our_rx[crypto_kx_SESSIONKEYBYTES];
	unsigned char our_tx[crypto_kx_SESSIONKEYBYTES];
	
	unsigned char partner_pk[crypto_kx_PUBLICKEYBYTES];
	
	/* create our keypair */
	crypto_kx_keypair(our_pk, our_sk);
	/* send our public key */
	send(sock, our_pk, crypto_kx_PUBLICKEYBYTES, 0);
	/* get partner's public key */
	recv(sock, partner_pk, crypto_kx_PUBLICKEYBYTES, 0);
	if(crypto_kx_client_session_keys(our_rx, our_tx,
				our_pk, our_sk, partner_pk) != 0) {
		fprintf(stderr,"suspicious partner public key.\n");
		exit(1);
	}
	/*
	 *
	 */
	sodium_bin2hex(hex, sizeof(hex), our_rx, sizeof(our_rx));
	printf("our_rx: %s\n", hex);
	sodium_bin2hex(hex, sizeof(hex), our_tx, sizeof(our_tx));
	printf("our_tx: %s\n", hex);

	unsigned char our_header[crypto_secretstream_xchacha20poly1305_HEADERBYTES];
	unsigned char partner_header[crypto_secretstream_xchacha20poly1305_HEADERBYTES];

	crypto_secretstream_xchacha20poly1305_state our_state;
	crypto_secretstream_xchacha20poly1305_state partner_state;

	/* tx stream */
	crypto_secretstream_xchacha20poly1305_init_push(&our_state, our_header, our_tx);
	send(sock, our_header, crypto_secretstream_xchacha20poly1305_HEADERBYTES, 0);
	/* rx stream*/
	recv(sock, partner_header, crypto_secretstream_xchacha20poly1305_HEADERBYTES, 0);
	/* Decrypt the stream: initializes the state, using the key and a header */
	if (crypto_secretstream_xchacha20poly1305_init_pull(&partner_state, partner_header, our_rx) != 0) {
		/* Invalid header, no need to go any further */
	}

	struct clone_args cl_args = {
		.flags        = CLONE_FILES,
		.pidfd        = (uint64_t)NULL,
		.child_tid    = (uint64_t)NULL,
		.parent_tid   = (uint64_t)NULL,
		.exit_signal  = SIGCHLD,
		.stack        = (uint64_t)NULL,
		.stack_size   = 0,
		.tls          = (uint64_t)NULL,
		.set_tid      = (uint64_t)NULL,
		.set_tid_size = 0,
		.cgroup       = 0,
	};
	pid_t pid = syscall(SYS_clone3, &cl_args, sizeof(cl_args));
	if(pid == 0) { /* child */
		send_till_quit(sock, &our_state);
		puts("shutdown the socket");
		shutdown(sock, SHUT_RDWR);
		exit(0);
	} else if(pid == -1) {
		perror("fork()");
		exit(1);
	}
	recv_till_exit(sock, &partner_state);
	kill(pid, SIGINT);
}

int main(int argc, char *argv[])
{
	arg0 = argv[0];
	if (argc != 2)
		usage();

	struct addrinfo hints, *network_info;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	int sock = get_sock_or_exit(hints, argv[1]);

	//TODO: client code here
	do_salt_things(sock);

	shutdown(sock, SHUT_RDWR);
	return 0;
}
