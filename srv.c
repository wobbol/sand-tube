#include <fcntl.h>
#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <inttypes.h>
#include <linux/sched.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sched.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <sodium.h>

const char *const port = "3490";
const int backlog = 10;

//TODO: write fail_perror()
void fail(const char *const fmt, ...){
	fprintf(stderr,"server: ");

	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	fprintf(stderr, "\n");

	exit(EXIT_FAILURE);
}

void reap(int sig)
{
	int saved_errno = errno;
	while(waitpid(-1, NULL, WNOHANG) > 0); /*XXX: when does this loop run more than once? */
	errno = saved_errno;
}

void install_child_reaper(void)
{
	struct sigaction sa;
	sa.sa_handler = reap;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if(sigaction(SIGCHLD, &sa, NULL) == -1)
		fail("sigaction()");
}

void clear_stdin(void) {
	int saved_status_flags = fcntl(STDIN_FILENO, F_GETFL);
	char buf;
	fcntl(STDIN_FILENO, F_SETFL, saved_status_flags | O_NONBLOCK);
	while(read(STDIN_FILENO, &buf, 1) != -1) {
		puts("chomp");
	}
	fcntl(STDIN_FILENO, F_SETFL, saved_status_flags);
}

void *get_in_addr(struct sockaddr *sa)
{
	if(sa->sa_family == AF_INET)
		return &(((struct sockaddr_in*)sa)->sin_addr);
	else
		return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int sockfd_or_fail(const char *port_str, struct addrinfo hints)
{
	struct addrinfo *servinfo;
	struct addrinfo *p;
	int sockfd;

	{
		int tmp;
		if((tmp = getaddrinfo(NULL, port_str, &hints, &servinfo)) != 0){
			fail("server: getaddrinfo: %s\n", gai_strerror(tmp));
		}
	}
	{
		const int yes = 1;
		for(p = servinfo; p != NULL; p = p->ai_next){
			if((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1){
				perror("server: socket()");
				continue;
			}

			if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes) == -1) {
				perror("server: setsockopt() could not set socket for reuse");
				continue;
			}

			if(bind(sockfd, p->ai_addr, p->ai_addrlen) == -1){
				close(sockfd);
				perror("server: bind()");
				continue;
			}
			break;
		}
	}

	freeaddrinfo(servinfo);

	if(p == NULL) {
		fail("No sutable internet connection.");
	}
	return sockfd;
}

/*
 * protocol overview
 *
 * . create keypair over unsecure conneciton
 *   . crypto_kx_keypair()
 *   . each party sends their public key to the other.
 *   . cryto_kx_server_session_keys() or cryto_kx_client_session_keys() depending on role
 *
 * . each party sends their
 *   header from crypto_secretstream_xchacha20poly1305_init_push()
 *   to the other
 * . each party uses the recieved header to initialize their state with
 *   crypto_secretstream_xchacha20poly1305_init_pull()
 * . each party can now decrypt recieved messages with
 *   crypto_secretstream_xchacha20poly1305_pull()
 * . each party can now encrypt messages with
 *   crypto_secretstream_xchacha20poly1305_push()
 *
 */
int send_till_quit(int conn, crypto_secretstream_xchacha20poly1305_state *state) {
	/* input buffer message buffer ciphertext buffer */
	unsigned char ibuf[64], mbuf[65], cbuf[128];

	clear_stdin();
	while(ibuf[0] != ':' && ibuf[1] != 'q') {
		/*
		 * fgets() always null terminates.
		 * fgets() always stores the whole string.
		 */
		unsigned char m_len = 0;

		memset(ibuf, 0xFF, sizeof(ibuf));
		puts("waiting on input");
		ssize_t r = read(STDIN_FILENO, ibuf, sizeof(ibuf) - 1);
		if(r == -1) {
			perror("read()");
			return 1;
		}
		ibuf[r] = '\0';
		printf("got input '%s'\n", ibuf);
		m_len = strlen(ibuf);
		assert(m_len < 65);
		mbuf[0] = m_len;
		memcpy(mbuf + 1, ibuf, sizeof(mbuf) - 1);

		crypto_secretstream_xchacha20poly1305_push
			(state, cbuf, NULL, mbuf, sizeof(mbuf), NULL, 0, 0);
		puts("sending");
		send(conn, cbuf, sizeof(mbuf) + crypto_secretstream_xchacha20poly1305_ABYTES , 0);
		//TODO: check for error on send and exit.
		puts("done sending");
		char hex[256];
		sodium_bin2hex(hex, sizeof(hex), cbuf, sizeof(mbuf) + crypto_secretstream_xchacha20poly1305_ABYTES);
		printf("sending: %s\n", hex);
	}
	puts("finished sending");
	return 0;
}

int recv_till_exit(int sock, crypto_secretstream_xchacha20poly1305_state *state) {
	while(1) {
		char hex[256];
		unsigned char tag;
		unsigned char buf[65], cbuf[82];
		unsigned long long mlen_p;
		size_t count = 0;
		int res = 0;
		puts("waiting for message");
		count = recv(sock, cbuf, sizeof(cbuf), 0);
		if(count ==  (size_t) -1 || count == 0) {
			perror("recv()");
			return 0;
		}
		printf("recv count %d\n", count);
		sodium_bin2hex(hex, sizeof(hex), cbuf, count);
		printf("received: %s\n", hex);

		res = crypto_secretstream_xchacha20poly1305_pull(
			state, buf, &mlen_p, &tag,
			cbuf, sizeof(buf) + crypto_secretstream_xchacha20poly1305_ABYTES,
			NULL, 0);
		if(res != 0) {
			puts("invalid ciphertext");
			/* Invalid/incomplete/corrupted ciphertext - abort */
		}
		unsigned char unpadded_len = buf[0];
		printf("\ntag: %d\nmlen_p: %d\nunpadded_len: %d\n", tag, mlen_p, unpadded_len);
		sodium_bin2hex(hex, sizeof(hex), buf + 1, unpadded_len);
		printf("hex: %s\ngot output '%.*s'\n", hex, unpadded_len, buf + 1);

		assert(tag == 0);
	fflush(stdout);
	}
	return 0;
}

void do_salt_stuff(int conn) {

	if(sodium_init() < 0) {
		fprintf(stderr, "libsodium could not init.\n");
	}
	char hex[256];

	unsigned char our_pk[crypto_kx_PUBLICKEYBYTES];
	unsigned char our_sk[crypto_kx_SECRETKEYBYTES];

	unsigned char our_rx[crypto_kx_SESSIONKEYBYTES];
	unsigned char our_tx[crypto_kx_SESSIONKEYBYTES];
	
	unsigned char partner_pk[crypto_kx_PUBLICKEYBYTES];
	
	/* create our keypair */
	crypto_kx_keypair(our_pk, our_sk);

	/* send our public key */
	send(conn, our_pk, crypto_kx_PUBLICKEYBYTES, 0);

	/* get partner's public key */
	int r = recv(conn, partner_pk, crypto_kx_PUBLICKEYBYTES, 0);
	if(r == -1) {
		perror("recv()");
	}

	if(crypto_kx_server_session_keys(our_rx, our_tx,
				our_pk, our_sk, partner_pk) != 0) {
		fprintf(stderr, "suspicious partner public key.\n");
		exit(1);
	}
	sodium_bin2hex(hex, sizeof(hex), our_rx, sizeof(our_rx));
	printf("our_rx: %s\n", hex);
	sodium_bin2hex(hex, sizeof(hex), our_tx, sizeof(our_tx));
	printf("our_tx: %s\n", hex);


	crypto_secretstream_xchacha20poly1305_state our_state;
	crypto_secretstream_xchacha20poly1305_state partner_state;
	unsigned char our_header[crypto_secretstream_xchacha20poly1305_HEADERBYTES];
	unsigned char partner_header[crypto_secretstream_xchacha20poly1305_HEADERBYTES];

	/* Set up a new stream: initialize the state and create the header */
	crypto_secretstream_xchacha20poly1305_init_push(&our_state, our_header, our_tx);
	send(conn, our_header, crypto_secretstream_xchacha20poly1305_HEADERBYTES, 0);

	/* rx stream */
	recv(conn, partner_header, crypto_secretstream_xchacha20poly1305_HEADERBYTES, 0);
	if(crypto_secretstream_xchacha20poly1305_init_pull(&partner_state, partner_header, our_rx) != 0) {
		/* invalid header */
	}
	struct clone_args cl_args = {
		.flags = CLONE_FILES,
		.pidfd = (uint64_t)NULL,
		.child_tid = (uint64_t)NULL,
		.parent_tid = (uint64_t)NULL,
		.exit_signal = SIGCHLD,
		.stack = (uint64_t)NULL,
		.stack_size = 0,
		.tls = (uint64_t)NULL,
		.set_tid = (uint64_t)NULL,
		.set_tid_size = 0,
		.cgroup = 0,
	};
	pid_t pid = syscall(SYS_clone3, &cl_args, sizeof(cl_args));
	if(pid == 0) { /* child */
		send_till_quit(conn, &our_state);
		puts("shutdown");
		shutdown(conn, SHUT_RDWR);
		exit(0);
	} else if(pid == -1) {
		perror("fork()");
		exit(1);
	}
	recv_till_exit(conn, &partner_state);
	kill(pid, SIGINT);

}
int main(void)
{
	struct addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	int sockfd = sockfd_or_fail(port, hints);
	socklen_t sin_size;

	if(listen(sockfd, backlog) == -1)
		fail("listen()");

	install_child_reaper();

	puts("server: waiting for connections.");
	while(1){
		int conn;
		struct sockaddr_storage their_addr;
		sin_size = sizeof their_addr;
		conn = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
		if(conn == -1){
			perror("server: accept()");
			continue;
		}
		char s[INET6_ADDRSTRLEN];
		inet_ntop(their_addr.ss_family,
				get_in_addr((struct sockaddr *)&their_addr), s, sizeof s);
		printf("server: connection from %s\n", s);
		if(!fork()){
			close(sockfd);

			do_salt_stuff(conn);

			puts("shutdown the socket");
			shutdown(conn, SHUT_RDWR);
			exit(EXIT_SUCCESS);
		}
		close(conn);
	}
	return 0;
}
