#include <sodium.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

void die(char *msg) {
	fprintf(stderr, "die: %s\n", msg);
	exit(0);
}
void usage(void) {
	printf("catchat fifo_in fifo_out\n");
}
int main(int argc, char *argv[]) {
	//if(argc < 3) {
	//	usage();
	//	return 0;
	//}
	if(sodium_init() < 0) {
		die("libsodium could not init.");
	}
	//int fifo_in  = open(argv[1], O_RDONLY);
	//int fifo_out = open(argv[2], O_WRONLY);
	int fifo_in = 3;
	int fifo_out = 4;


	if(fifo_in < 0 || fifo_out < 0) {
		die("fifo failure");
	}
	unsigned char our_pk[crypto_kx_PUBLICKEYBYTES];
	unsigned char our_sk[crypto_kx_SECRETKEYBYTES];

	unsigned char our_rx[crypto_kx_SESSIONKEYBYTES];
	unsigned char our_tx[crypto_kx_SESSIONKEYBYTES];
	
	unsigned char partner_pk[crypto_kx_PUBLICKEYBYTES];
	
	/* create our keypair */
	crypto_kx_keypair(our_pk, our_sk);
	write(fifo_out, our_pk, crypto_kx_PUBLICKEYBYTES);
	read(fifo_in, partner_pk, crypto_kx_PUBLICKEYBYTES);
	if(crypto_kx_client_session_keys(our_rx, our_tx,
				our_pk, our_sk, partner_pk) != 0) {
		die("suspicious partner public key.");
	}

	/* get partner's public key */
	//todo: something here
	
	/* use:
	 *      crypto_secretbox_*()
	 *      crypto_secretstream_*()
	 *      crypto_aead_*()
	 * to decrypt and encrypt data.
	 * */
	return 0;
}
